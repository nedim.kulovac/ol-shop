import { CircularProgress } from '@material-ui/core';
import { useRouter } from 'next/router';
import { useEffect } from 'react';

export default function Error404() {
  const router = useRouter();
  useEffect(() => {
    router.push('/auth/[form]', '/auth/404');
  }, []);
  return (
    <div
      style={{
        display: 'flex',
        height: '100vh',
        justifyContent: 'center',
        alignItems: 'center',
      }}
    >
      <CircularProgress />
    </div>
  );
}
