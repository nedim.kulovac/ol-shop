import { CircularProgress } from '@material-ui/core';
import { useRouter } from 'next/router';
import { useEffect } from 'react';

function Error() {
  const router = useRouter();
  useEffect(() => {
    router.push('/auth/[form]', '/auth/error');
  }, []);
  return (
    <div
      style={{
        display: 'flex',
        height: '100vh',
        justifyContent: 'center',
        alignItems: 'center',
      }}
    >
      <CircularProgress />
    </div>
  );
}

export default Error;
