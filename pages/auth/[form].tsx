import { useRouter } from 'next/router';

import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import Signin from 'src/components/auth/sign-in';
import Signup from 'src/components/auth/sign-up';
import ForgotPassword from 'src/components/auth/forgot-password';
import ConfirmRegistration from 'src/components/auth/confirm-registration';
import { Context } from '@apollo/client';
import ChangePassword from 'src/components/auth/change-password';
import Logout from 'src/components/auth/sign-out';
import Custom404 from 'src/components/custom-404';
import CustomError from 'src/components/custom-error';

const useStyles = makeStyles((theme) => ({
  root: {
    flex: 1,
    height: '100vh',
  },
  logo: {},
  form: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
}));

const FormContent = ({ email, randString }) => {
  const router = useRouter();
  switch (router.query.form) {
    case 'sign-up':
      return <Signup />;
    case 'sign-in':
      return <Signin />;
    case 'forgot-password':
      return <ForgotPassword />;
    case 'confirm-registration':
      return <ConfirmRegistration email={email} randString={randString} />;
    case 'change-password':
      return <ChangePassword />;
    case 'sign-out':
      return <Logout />;
    case '404':
      return <Custom404 />;
    case 'error':
      return <CustomError />;
    default:
      return <Custom404 />;
  }
};

const Form = (props: { email: string; randString: string }) => {
  const router = useRouter();
  const classes = useStyles();

  return (
    <Grid container className={classes.root}>
      <Grid item xs={12} sm={4} md={7} className={classes.logo}>
        <div>{router.query.form}</div>
      </Grid>
      <Grid item xs={12} sm={8} md={5} className={classes.form}>
        {<FormContent email={props.email} randString={props.randString} />}
      </Grid>
    </Grid>
  );
};

export default Form;

Form.getInitialProps = async (context: Context) => {
  const { query } = context;
  return {
    email: query.email,
    randString: query.randString,
  };
};
