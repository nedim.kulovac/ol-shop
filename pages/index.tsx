import { useEffect, useContext } from 'react';
import { useRouter } from 'next/router';
// import Link from 'next/link';
import { gql, useQuery } from '@apollo/client';
import { AppContext } from 'src/contexts/AppContext';
// import { AuthContext } from "contexts/AuthContext";
// import { AppContext } from "../contexts/AuthContext";
import { Types } from '../src/contexts/reducers';
import { CurrentUser } from '../src/gql-helpers/queries';

import { i18n, Link, withTranslation } from '../i18n';
import LanguageSwitcher from 'src/components/shared/language-switcher';

// const CurrentUser = gql`
//   query CurrentUserQuery {
//     currentUser {
//       _id
//       firstName
//       lastName
//       email
//       tempPassReq
//     }
//   }
// `;

const Index = ({ t }) => {
  // const { isAuthenticated, setAuthStatus } = useContext(AuthContext);
  // console.log("IND ", isAuthenticated);

  // const { authenticatedUser, authActionTypes, dispatch } = useContext(
  //   AuthContext
  // );

  // t = 1;
  const { state, dispatch } = useContext(AppContext);

  const router = useRouter();
  const { data, loading, error } = useQuery(CurrentUser);
  const currentUser = data?.currentUser;

  const shouldRedirect = !(loading || error || currentUser);
  useEffect(() => {
    if (data?.currentUser) {
      dispatch({
        type: Types.SetAuthenticatedUser,
        payload: data.currentUser,
      });
    }
    // set feched user to context
    // dispatch({
    //   type: authActionTypes.setAuth,
    //   authenticatedUser: data.currentUser,
    // });
  }, [data]);

  useEffect(() => {
    if (shouldRedirect) {
      router.push('/sign-in');
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [shouldRedirect]);

  if (error) {
    return (
      <div>
        <div style={{ display: 'flex', justifyContent: 'center' }}>
          <LanguageSwitcher />
        </div>
        <p>{error.message}</p>;
        <div>
          <Link href="/auth/sign-in">
            <a>Sign in</a>
          </Link>
          <br />
          <Link href="/auth/sign-up">
            <a>Sign up</a>
          </Link>
          <Link href="/auth/[form]" as={`/auth/sign-in`}>
            <a>First Post</a>
          </Link>
        </div>
      </div>
    );
  }

  if (currentUser) {
    return (
      <div>
        {JSON.stringify(data, null, 4)}
        <Link href="/about">
          <a>about</a>
        </Link>{' '}
        page. or{' '}
        <Link href="/auth/[form]" as={`/auth/sign-out`}>
          <a>signout</a>
        </Link>
        <Link href="/auth/[form]" as="/auth/change-password">
          <a>change pass</a>
        </Link>
        <pre>CurrentUser: {JSON.stringify(data?.currentUser, null, 4)}</pre>
        <pre>
          Auth User: {JSON.stringify(state.auth?.authenticatedUser, null, 4)}
        </pre>
      </div>
    );
  }

  return <p>Loading...</p>;
};

Index.getInitialProps = async () => ({
  namespacesRequired: ['common'],
});

export default withTranslation()(Index);

// import Head from 'next/head'
// import styles from '../styles/Home.module.css'
// import { makeStyles } from '@material-ui/core/styles';
// import Button from '@material-ui/core/Button';
// import { gql, useQuery } from '@apollo/client';
// import { initializeApollo } from 'lib/apollo'

// const useStyles = makeStyles((theme) => ({
//   root: {
//     '& > *': {
//       margin: theme.spacing(1),
//     },
//   },
// }));

// const GET_CURRENT_USER = gql`
//   query GetUser {
//     user {
//       _id
//       email
//     }
//   }
// `;

// const Home = ({ name }) => {
//   const classes = useStyles()
//   const { loading, error, data } = useQuery(GET_CURRENT_USER, { variables: { classes }, skip: !classes });

//   if (loading) return 'Loading...';
//   if (error) return `Error! ${error.message}`;

//   return (
//     <div className={styles.container}>
//       <Head>
//         <title>Create Next App</title>
//         <link rel="icon" href="/favicon.ico" />
//       </Head>

//       <div>Pulled out from props: {name}</div>
//       <div className={classes.root}>
//         <Button variant="contained" >Default</Button>
//         <Button variant="contained" color="primary">Primary</Button>
//         <Button variant="contained" color="secondary"> Secondary</Button>
//         <Button variant="contained" disabled>Disabled</Button>
//         <Button variant="contained" color="primary" href="#contained-buttons">Link</Button>
//       </div>
//       <pre>
//         {JSON.stringify(data, null, 4)}
//       </pre>
//     </div>
//   )
// }

// export async function getStaticProps() {
//   const apolloClient = initializeApollo();

//   await apolloClient.query({
//     query: GET_CURRENT_USER,
//   });

//   return {
//     props: {
//       initialApolloState: apolloClient.cache.extract(),
//     }
//   }
// }

// export default Home;
