import { HttpStatusCode } from './HttpStatusCode';

export class BaseError extends Error {
  public readonly httpCode: HttpStatusCode;
  public readonly message: string;

  constructor(httpCode: HttpStatusCode, message: string) {
    super(message);
    Object.setPrototypeOf(this, new.target.prototype);

    this.httpCode = httpCode;
    this.message = message;

    Error.captureStackTrace(this);
  }
}
