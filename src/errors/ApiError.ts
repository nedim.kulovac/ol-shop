import { BaseError } from './BaseError';
import { HttpStatusCode } from './HttpStatusCode';

export class ApiError extends BaseError {
  constructor(
    httpCode = HttpStatusCode.INTERNAL_SERVER,
    message = 'internal server error'
  ) {
    super(httpCode, message);
  }
}
