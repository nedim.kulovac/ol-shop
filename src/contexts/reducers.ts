import { UserInterface } from "src/interfaces";

type ActionMap<M extends { [index: string]: any }> = {
  [Key in keyof M]: M[Key] extends undefined
    ? {
        type: Key;
      }
    : {
        type: Key;
        payload: M[Key];
      };
};

export enum Types {
  SetAuthenticatedUser = "SET_AUTHENTICATED_USER",
  //   SetTempData = "SET_TEMP_DATA",
}

type AuthPayload = {
  [Types.SetAuthenticatedUser]: UserInterface;
  //   [Types.SetTempData]: {
  //     email: string;
  //     tempPassword: string;
  //   };
};

export type AuthActions = ActionMap<AuthPayload>[keyof ActionMap<AuthPayload>];

export const authReducer = (state: UserInterface, action: AuthActions) => {
  switch (action.type) {
    case Types.SetAuthenticatedUser:
      return {
        ...state,
        authenticatedUser: action.payload,
      };
    // case Types.SetTempData:
    //   return {
    //     ...state,
    //     tempData: action.payload,
    //   };
  }
};
