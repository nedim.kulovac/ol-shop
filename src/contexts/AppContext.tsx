import React, { createContext, useReducer, Dispatch } from "react";
import { UserInterface } from "src/interfaces";
import { AuthActions, authReducer } from "./reducers";

type InitialStateType = {
  auth;
};

const initialState = {
  auth: null,
};

const AppContext = createContext<{
  state: InitialStateType;
  dispatch: Dispatch<AuthActions>;
}>({
  state: initialState,
  dispatch: () => null,
});

const mainReducer = ({ auth }: InitialStateType, action: AuthActions) => ({
  auth: authReducer(auth, action),
});

const AppProvider: React.FC = ({ children }) => {
  const [state, dispatch] = useReducer(mainReducer, initialState);

  return (
    <AppContext.Provider value={{ state, dispatch }}>
      {children}
    </AppContext.Provider>
  );
};

export { AppProvider, AppContext };
