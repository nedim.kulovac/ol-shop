import { gql } from '@apollo/client';

export const SigninMutation = gql`
  mutation SigninMutation(
    $email: String!
    $password: String!
    $rcToken: String!
  ) {
    signin(email: $email, password: $password, rcToken: $rcToken) {
      _id
      firstName
      lastName
      email
      tempPassReq
    }
  }
`;

export const SignupMutation = gql`
  mutation SignupMutation(
    $firstName: String!
    $lastName: String!
    $email: String!
    $password: String!
    $rcToken: String!
    $language: String!
  ) {
    signup(
      firstName: $firstName
      lastName: $lastName
      email: $email
      password: $password
      rcToken: $rcToken
      language: $language
    ) {
      _id
    }
  }
`;

export const ResetPasswordMutation = gql`
  mutation ResetPasswordMutation(
    $email: String!
    $rcToken: String!
    $language: String!
  ) {
    resetPassword(email: $email, rcToken: $rcToken, language: $language)
  }
`;

export const ChangePasswordMutation = gql`
  mutation ChangePasswordMutation(
    $email: String!
    $password: String!
    $newPassword: String!
    $flag: String!
  ) {
    changePassword(
      email: $email
      password: $password
      newPassword: $newPassword
      flag: $flag
    )
  }
`;

export const ConfirmUserRegistration = gql`
  mutation ConfirmUserRegistrationMutation(
    $email: String!
    $randString: String!
  ) {
    confirmUserRegistration(email: $email, randString: $randString)
  }
`;

export const SigoutMutation = gql`
  mutation SigoutMutation {
    signout
  }
`;
