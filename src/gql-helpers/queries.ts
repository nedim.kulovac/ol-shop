import { gql } from '@apollo/client';

export const CurrentUser = gql`
  query CurrentUserQuery {
    currentUser {
      _id
      firstName
      lastName
      email
      tempPassReq
    }
  }
`;
