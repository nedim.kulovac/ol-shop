import mongoose from "mongoose";

const connection: any = {};

async function dbConnect() {
    if (connection.isConnected) return connection.isConnected;
    const db = await mongoose.connect(process.env.MONGO_URI, {
        useNewUrlParser: true,
        useUnifiedTopology: true
    });
    connection.isConnected = db.connections[0].readyState;
}
export default dbConnect;