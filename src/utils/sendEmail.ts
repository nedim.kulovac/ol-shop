export const templates = {
  password_reset_confirm: 'd-x',
  password_reset_en: 'd-3d90086d50fc496f8a9ace831d79a8e4',
  password_reset_bs: 'd-05e4d229936c4e4a98764dcc7592d38b',
  confirm_account_en: 'd-ace34bb713914726ba9581f9635081db',
  confirm_account_bs: 'd-96a340220e0a47e79784258694698df5',
};

export const sendEmail = async (data) => {
  const sgMail = require('@sendgrid/mail');
  sgMail.setApiKey(process.env.SENDGRID_API_KEY);
  const msg = {
    to: data.email,
    from: 'ep.app.setup@gmail.com',
    templateId: templates[data.templateName],

    dynamic_template_data: { ...data },
  };
  sgMail.send(msg);
};
