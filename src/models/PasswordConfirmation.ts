import mongoose from 'mongoose';
import { PasswordConfirmationInterface } from 'src/interfaces';

const Schema = mongoose.Schema;

const PasswordConfirmationSchema = new Schema({
  email: {
    type: String,
    required: true,
  },
  randString: {
    type: String,
    required: true,
  },
  expirationDate: {
    type: String,
    required: true,
  },
});

const PasswordConfirmation =
  mongoose.models.PasswordConfirmation ||
  mongoose.model<PasswordConfirmationInterface>(
    'PasswordConfirmation',
    PasswordConfirmationSchema
  );
export default PasswordConfirmation;
