import mongoose from 'mongoose';
import { TempPasswordInterface } from 'src/interfaces';

const Schema = mongoose.Schema;

const TempPasswordSchema = new Schema({
  email: {
    type: String,
    required: true,
  },
  tempPassword: {
    type: String,
    required: true,
  },
  expirationDate: {
    type: String,
    required: true,
  },
});

const TempPassword =
  mongoose.models.TempPassword ||
  mongoose.model<TempPasswordInterface>('TempPassword', TempPasswordSchema);
export default TempPassword;
