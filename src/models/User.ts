import mongoose from 'mongoose';
import { UserInterface } from 'src/interfaces';

const Schema = mongoose.Schema;

const userSchema = new Schema(
  {
    email: {
      type: String,
      required: true,
      unique: true,
    },
    firstName: {
      type: String,
    },
    lastName: {
      type: String,
    },
    password: {
      type: String,
      required: true,
    },
    creator: {
      type: Object,
    },
    verified: {
      type: Boolean,
      required: true,
    },
    tempPassReq: {
      type: Boolean,
      required: true,
    },
  },
  { timestamps: true }
);

const User =
  mongoose.models.User || mongoose.model<UserInterface>('User', userSchema);
export default User;
