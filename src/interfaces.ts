import mongoose from 'mongoose';

export interface UserInterface extends mongoose.Document {
  _id: string;
  email: string;
  firstName: string;
  lastName: string;
  password: string;
  creator: string;
  createdAt: string;
  updatedAt: string;
  verified: boolean;
  tempPassReq: boolean;
}

export interface PasswordConfirmationInterface extends mongoose.Document {
  email: string;
  randString: string;
  expirationDate: string;
}

export interface TempPasswordInterface extends mongoose.Document {
  email: string;
  tempPassword: string;
  expirationDate: string;
}

export interface SigninPayload {
  email: string;
  password: string;
  rcToken: string;
}

export interface ResetPasswordPayload {
  email: string;
  rcToken: string;
  language: string;
}

export interface ConfirmUserRegistrationPayload {
  email: string;
  randString: string;
}

export interface ChangePasswordPayload {
  email: string;
  password: string;
  newPassword: string;
  confirmPassword: string;
  flag?: string;
}

export interface SignupPayload {
  firstName: string;
  lastName: string;
  email: string;
  password: string;
  confirmPassword: string;
  rcToken: string;
  language: string;
}
