import { objectType } from '@nexus/schema';

export const User = objectType({
    name: "User",
    definition(t) {
        t.string("_id"),
        t.string("email"),
        t.string("firstName"),
        t.string("lastName"),
        t.string("password", {
            nullable: true
        }),
        t.string("creator", {
            nullable: true
        }),
        t.int("createdAt", {
            nullable: true
        }),
        t.int("updatedAt", {
            nullable: true
        }),
        t.boolean("verified", {
            nullable: true
        }),
        t.boolean("tempPassReq", {
            nullable: true
        })
    }
});


