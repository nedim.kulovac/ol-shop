import { queryType } from '@nexus/schema';
import { User } from './index';

import { findUser } from 'src/services/userService';
import { getLoginSession } from 'src/services/auth';
import { ApiError } from 'next/dist/next-server/server/api-utils';
import { HttpStatusCode } from 'src/errors/HttpStatusCode';

export const Query = queryType({
  definition(t) {
    t.field('currentUser', {
      type: User,
      resolve: async (parent, args, ctx: any) => {
        const session = await getLoginSession(ctx.req);
        if (session) {
          const user = findUser(session.email);
          return user;
        }
        throw new ApiError(
          HttpStatusCode.BAD_REQUEST,
          'Authentication token is invalid'
        );
      },
      description: 'Returns currently signed in user',
    });
  },
});
