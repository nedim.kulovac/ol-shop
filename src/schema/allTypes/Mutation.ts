import { mutationType, stringArg } from '@nexus/schema';
import { User } from './index';
import {
  handleUserSignin,
  handleUserSignUp,
  confirmRegistration,
  resetPassword,
  changePassword,
} from 'src/services/userService';
import {
  SigninPayload,
  ResetPasswordPayload,
  ConfirmUserRegistrationPayload,
  ChangePasswordPayload,
  SignupPayload,
} from '../../interfaces';
import { removeTokenCookie } from 'lib/auth-cookies';
import { Context } from '@apollo/client';

export const Mutation = mutationType({
  definition(t) {
    t.field('signup', {
      type: User,
      args: {
        firstName: stringArg(),
        lastName: stringArg(),
        email: stringArg(),
        password: stringArg(),
        rcToken: stringArg(),
        language: stringArg(),
      },
      resolve: async (parent, args: SignupPayload, ctx) => {
        return handleUserSignUp({ ...args });
      },
      description: 'User Sign Up handler',
    }),
      t.field('signin', {
        type: User,
        args: {
          email: stringArg(),
          password: stringArg(),
          rcToken: stringArg(),
        },
        resolve: async (parent, args: SigninPayload, ctx: Context) => {
          return handleUserSignin({ ...args }, ctx);
        },
        description: 'User Sign In handler',
      }),
      t.field('signout', {
        type: 'Boolean',
        resolve: (parent, args, ctx: any) => {
          removeTokenCookie(ctx.res);
          return true;
        },
        description: 'Sign out the user and invalidate the session',
      }),
      t.field('confirmUserRegistration', {
        type: 'Boolean',
        args: { email: stringArg(), randString: stringArg() },
        resolve: (parent, args: ConfirmUserRegistrationPayload, ctx: any) => {
          return confirmRegistration({ ...args });
        },
        description: 'Mail confirmation',
      }),
      t.field('resetPassword', {
        type: 'Boolean',
        args: {
          email: stringArg(),
          rcToken: stringArg(),
          language: stringArg(),
        },
        resolve: (parent, args: ResetPasswordPayload, ctx: any) => {
          return resetPassword({ ...args });
        },
        description: 'Password reset mutation',
      }),
      t.field('changePassword', {
        type: 'Boolean',
        args: {
          email: stringArg(),
          password: stringArg(),
          newPassword: stringArg(),
          flag: stringArg(),
        },
        resolve: (parent, args: ChangePasswordPayload, ctx: any) => {
          return changePassword({ ...args });
        },
        description: 'Change password mutation',
      });
  },
});
