import dbConnect from 'src/utils/dbConnect';
import User from 'src/models/User';
import PasswordConfirmation from 'src/models/PasswordConfirmation';
import TempPassword from 'src/models/TempPassword';
const bcrypt = require('bcrypt');

import { sendEmail } from 'src/utils/sendEmail';
import { HttpStatusCode } from 'src/errors/HttpStatusCode';
import { ApiError } from 'src/errors/ApiError';

import { setLoginSession } from 'src/services/auth';
import {
  ChangePasswordPayload,
  ConfirmUserRegistrationPayload,
  SigninPayload,
  ResetPasswordPayload,
  SignupPayload,
} from 'src/interfaces';
import {
  PasswordConfirmationInterface,
  TempPasswordInterface,
  UserInterface,
} from 'src/interfaces';
import { Context } from '@apollo/client';

const saltRounds: number = 10;
const expirationTime: number = 300000; // 5 minutes
var crypto = require('crypto');

export const handleUserSignUp = async ({
  firstName,
  lastName,
  email,
  password,
  rcToken,
  language,
}: SignupPayload): Promise<UserInterface> => {
  const human = await validateHuman(rcToken);
  if (!human) throw new ApiError(HttpStatusCode.BAD_REQUEST, 'Bot spotted!');

  const user: UserInterface = await createUser(
    firstName,
    lastName,
    email,
    password,
    language
  );
  if (!user)
    throw new ApiError(HttpStatusCode.BAD_REQUEST, 'Your registration failed');
  return user;
};

export const handleUserSignin = async (
  { email, password, rcToken }: SigninPayload,
  ctx: Context
): Promise<UserInterface> => {
  const human = await validateHuman(rcToken);
  if (!human) throw new ApiError(HttpStatusCode.BAD_REQUEST, 'Bot spotted!');

  const user: UserInterface = await findUser(email);
  // Check if temporary password is required by user (forgot password)
  if (user && user.tempPassReq) {
    const tempPassword = await TempPassword.findOne({ email });
    if (
      tempPassword &&
      new Date(Date.now()).toISOString() > tempPassword.expirationDate
    ) {
      TempPassword.deleteOne({ email });
      throw new ApiError(
        HttpStatusCode.BAD_REQUEST,
        'Confirmation time expired'
      );
    }
    if (user.email === email && tempPassword.tempPassword === password) {
      return user;
    }
    throw new ApiError(HttpStatusCode.BAD_REQUEST, 'Wrong credentials');
  }

  // regular signin
  if (user && (await validatePassword(user, password)) && user.verified) {
    const session = {
      id: user._id,
      email: user.email,
    };
    await setLoginSession(ctx.res, session);
    return user;
  } else if (
    user &&
    (await validatePassword(user, password)) &&
    !user.verified
  ) {
    throw new ApiError(
      HttpStatusCode.BAD_REQUEST,
      'Please verify your account by clicking the link in the email we sent you'
    );
  } else {
    throw new ApiError(HttpStatusCode.BAD_REQUEST, 'Wrong credentials!');
  }
};

export async function validateHuman(rcToken: string): Promise<boolean> {
  const secret = process.env.RECAPTCHA_SECRET_KEY;
  const response = await fetch(
    `https://www.google.com/recaptcha/api/siteverify?secret=${secret}&response=${rcToken}`,
    {
      method: 'POST',
    }
  );
  const data = await response.json();
  return data.success;
}

export async function createUser(
  firstName: string,
  lastName: string,
  email: string,
  password: string,
  language: string
): Promise<UserInterface> {
  dbConnect();
  const userExist = await User.findOne({ email });
  if (userExist) throw new ApiError(HttpStatusCode.CONFLICT, `email in use`);

  const user = new User({
    firstName,
    lastName,
    email: email,
    password: await bcrypt.hash(password, saltRounds),
    verified: false,
    tempPassReq: false,
  });
  const userOnSuccess = await user.save();
  if (!userOnSuccess)
    throw new ApiError(HttpStatusCode.INTERNAL_SERVER, 'Registration failed');

  const passConfirmation = await storePasswordConfirmationInfo(email);

  const dynamicData = {
    name: firstName,
    email,
    confirm_account_url: `http://localhost:3000/auth/confirm-registration?email=${email}&randString=${passConfirmation.randString}`,
    templateName: `confirm_account_${language}`,
  };
  sendEmail(dynamicData);
  return userOnSuccess;
}

async function storePasswordConfirmationInfo(
  email: string
): Promise<PasswordConfirmationInterface> {
  var randString = crypto.randomBytes(20).toString('hex');
  const expirationDate = new Date(Date.now() + expirationTime).toISOString();
  const passwordConfirmation = new PasswordConfirmation({
    email,
    randString,
    expirationDate,
  });
  return passwordConfirmation.save();
}

export async function resetPassword({
  email,
  rcToken,
  language,
}: ResetPasswordPayload): Promise<boolean> {
  const human = await validateHuman(rcToken);
  if (!human) throw new ApiError(HttpStatusCode.BAD_REQUEST, 'Bot spotted!');

  dbConnect();
  const user = await User.findOne({ email });
  if (!user) throw new ApiError(HttpStatusCode.BAD_REQUEST, `email not found`);

  const tempPassInDB = await TempPassword.findOne({ email });

  const tempPassword = new TempPassword({
    email,
    tempPassword: crypto.randomBytes(20).toString('hex').substring(0, 8),
    expirationDate: new Date(Date.now() + 300 * 1000).toISOString(), // 5 minutes
  });
  const tempPassSaved = tempPassInDB
    ? await TempPassword.updateOne(
        { email },
        {
          tempPassword: tempPassword.tempPassword,
          expirationDate: tempPassword.expirationDate,
        }
      )
    : await tempPassword.save();
  const updatedUser = await User.updateOne({ email }, { tempPassReq: true });

  if (tempPassSaved && updatedUser) {
    const dynamicData = {
      name: user.firstName,
      tempPassword: tempPassword.tempPassword,
      email,
      templateName: `password_reset_${language}`,
    };
    sendEmail(dynamicData);
    return true;
  } else {
    throw new ApiError(
      HttpStatusCode.INTERNAL_SERVER,
      `Error occured - generic`
    );
  }
}

export async function validatePassword(
  user: UserInterface,
  inputPassword: string
): Promise<boolean> {
  dbConnect();
  return await bcrypt.compare(inputPassword, user.password);
}

export async function confirmRegistration({
  email,
  randString,
}: ConfirmUserRegistrationPayload): Promise<boolean> {
  dbConnect();
  if (!email || !randString)
    throw new ApiError(HttpStatusCode.BAD_REQUEST, 'Error occured - generic');
  const user = await User.findOne({ email });
  if (user && user.verified)
    throw new ApiError(HttpStatusCode.BAD_REQUEST, `email is already verified`);
  if (!user) throw new ApiError(HttpStatusCode.BAD_REQUEST, `no account`);

  const confirmationData = await PasswordConfirmation.findOne({ email });
  if (
    confirmationData &&
    email === confirmationData.email &&
    randString === confirmationData.randString &&
    new Date(Date.now()).toISOString() < confirmationData.expirationDate
  ) {
    await User.updateOne({ email }, { verified: true });
    await PasswordConfirmation.deleteOne({ email });
    return true;
  } else if (
    confirmationData &&
    new Date(Date.now()).toISOString() > confirmationData.expirationDate
  ) {
    await User.deleteOne({ email });
    await PasswordConfirmation.deleteOne({ email });
    throw new ApiError(HttpStatusCode.BAD_REQUEST, `register again`);
  } else {
    await User.deleteOne({ email });
    await PasswordConfirmation.deleteOne({ email });
    throw new ApiError(
      HttpStatusCode.INTERNAL_SERVER,
      `Error occured - generic`
    );
  }
}

export async function changePassword({
  email,
  password,
  newPassword,
  flag,
}: ChangePasswordPayload): Promise<boolean> {
  if (flag === 'forgotPassword') {
    const tempPass: TempPasswordInterface = await TempPassword.findOne({
      email,
    });
    if (!tempPass)
      throw new ApiError(
        HttpStatusCode.INTERNAL_SERVER,
        `Error occured - generic`
      );
    if (
      tempPass &&
      new Date(Date.now()).toISOString() > tempPass.expirationDate
    )
      throw new ApiError(
        HttpStatusCode.BAD_REQUEST,
        `forgot pass confirmation time expired`
      );
    if (tempPass && tempPass.tempPassword !== password)
      throw new ApiError(HttpStatusCode.BAD_REQUEST, `password mismatch`);
    if (
      tempPass &&
      tempPass.tempPassword === password &&
      new Date(Date.now()).toISOString() < tempPass.expirationDate
    ) {
      const hashedPassword = await bcrypt.hash(newPassword, saltRounds);
      await User.updateOne(
        { email },
        {
          password: hashedPassword,
          tempPassReq: false,
        }
      );
      await TempPassword.deleteOne({ email });
      return true;
    }
  } else if (flag === 'changePassword') {
    const user = await User.findOne({ email });
    if (!user) throw new ApiError(HttpStatusCode.BAD_REQUEST, `no user found`);
    const hashedPassword = await bcrypt.hash(newPassword, saltRounds);
    if (user && user.password === hashedPassword)
      throw new ApiError(HttpStatusCode.BAD_REQUEST, `same pass`);
    const isPasswordValid = await validatePassword(user, password);
    if (user && !isPasswordValid)
      throw new ApiError(HttpStatusCode.BAD_REQUEST, `Wrong credentials!`);
    if (user && isPasswordValid) {
      const saved = await User.updateOne(
        { email },
        { password: await bcrypt.hash(newPassword, saltRounds) }
      );
      if (!saved)
        throw new ApiError(
          HttpStatusCode.INTERNAL_SERVER,
          `Error occured - generic`
        );
      if (saved) return true;
    }
  }
  throw new ApiError(HttpStatusCode.INTERNAL_SERVER, `Error occured - generic`);
}

export async function findUser(email) {
  dbConnect();
  const user = await User.findOne({ email: email });
  return user;
}
