import { withStyles, Theme } from '@material-ui/core/styles';
import { Tooltip } from '@material-ui/core';

const LightTooltip = withStyles((theme: Theme) => ({
  tooltip: {
    color: '#FFF',
    boxShadow: theme.shadows[1],
    fontSize: 14,
    marginLeft: 0,
  },
}))(Tooltip);

export default LightTooltip;
