import React from 'react';
import { useTranslation } from 'react-i18next';

import { makeStyles, withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import MuiListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import DashboardIcon from '@material-ui/icons/Dashboard';
import SettingsIcon from '@material-ui/icons/Settings';
import PeopleIcon from '@material-ui/icons/People';
import { Collapse } from '@material-ui/core';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import AccountTreeIcon from '@material-ui/icons/AccountTree';
import AppsIcon from '@material-ui/icons/Apps';
import ReorderIcon from '@material-ui/icons/Reorder';

import LightTooltip from './tooltip';

interface State {
  selectedIndex: string;
  prodMngtSublistOpened: boolean;
}

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    maxWidth: 360,
  },
  dashboardIcon: {
    color: '#85898f',
  },
  nested: {
    paddingLeft: theme.spacing(4),
  },
}));

const ListItem = withStyles({
  root: {
    '&$selected': {
      backgroundColor: '#121820',
      color: 'white',
    },
    '&$selected:hover': {
      backgroundColor: '#121820',
      color: 'white',
    },
    '&:hover': {
      backgroundColor: '#192029',
      color: 'white',
    },
  },
  selected: {},
})(MuiListItem);

const MainList = ({ open, toggleDrawer }: any) => {
  const classes = useStyles();
  const { t } = useTranslation('common');

  const initalState: State = {
    selectedIndex: 'dashboard',
    prodMngtSublistOpened: false,
  };
  const reducer = (state: State, action: { type: string }): State => {
    switch (action.type) {
      case 'dashboard':
        return { ...state, selectedIndex: 'dashboard' };
      case 'productsManagement':
        return {
          ...state,
          selectedIndex: 'productsManagement',
          prodMngtSublistOpened: !state.prodMngtSublistOpened,
        };
      case 'categories':
        return { ...state, selectedIndex: 'categories' };
      case 'products':
        return { ...state, selectedIndex: 'products' };
      case 'users':
        return { ...state, selectedIndex: 'users' };
      case 'settings':
        return { ...state, selectedIndex: 'settings' };

      default:
        return state;
    }
  };
  const [state, dispatch] = React.useReducer(reducer, initalState);
  const { selectedIndex, prodMngtSublistOpened } = state;

  return (
    <div className={classes.root}>
      <List component="nav" aria-label="main mailbox folders">
        <LightTooltip title={open ? '' : t('Dashboard')} placement="right">
          <div onClick={toggleDrawer ? toggleDrawer(false) : null}>
            <ListItem
              button
              selected={selectedIndex === 'dashboard'}
              onClick={() => {
                dispatch({ type: 'dashboard' });
              }}
            >
              <ListItemIcon>
                <DashboardIcon className={classes.dashboardIcon} />
              </ListItemIcon>
              <ListItemText primary={t('Dashboard')} />
            </ListItem>
          </div>
        </LightTooltip>

        <LightTooltip title={open ? '' : t('Products Mngt')} placement="right">
          <ListItem
            button
            selected={selectedIndex === 'productsManagement'}
            onClick={() => dispatch({ type: 'productsManagement' })}
          >
            <ListItemIcon>
              <AccountTreeIcon className={classes.dashboardIcon} />
            </ListItemIcon>
            <ListItemText primary={t('Products Mngt')} />
            {prodMngtSublistOpened ? <ExpandLess /> : <ExpandMore />}
          </ListItem>
        </LightTooltip>
        <Collapse in={prodMngtSublistOpened} timeout="auto" unmountOnExit>
          <List component="div" disablePadding>
            <LightTooltip title={open ? '' : t('Categories')} placement="right">
              <div onClick={toggleDrawer ? toggleDrawer(false) : null}>
                <ListItem
                  button
                  className={classes.nested}
                  selected={selectedIndex === 'categories'}
                  onClick={() => dispatch({ type: 'categories' })}
                >
                  <ListItemIcon>
                    <AppsIcon className={classes.dashboardIcon} />
                  </ListItemIcon>
                  <ListItemText primary={t('Categories')} />
                </ListItem>
              </div>
            </LightTooltip>
            <LightTooltip title={open ? '' : t('Products')} placement="right">
              <div onClick={toggleDrawer ? toggleDrawer(false) : null}>
                <ListItem
                  button
                  className={classes.nested}
                  selected={selectedIndex === 'products'}
                  onClick={() => dispatch({ type: 'products' })}
                >
                  <ListItemIcon>
                    <ReorderIcon className={classes.dashboardIcon} />
                  </ListItemIcon>
                  <ListItemText primary={t('Products')} />
                </ListItem>
              </div>
            </LightTooltip>
          </List>
        </Collapse>

        <LightTooltip title={open ? '' : t('Users')} placement="right">
          <div onClick={toggleDrawer ? toggleDrawer(false) : null}>
            <ListItem
              button
              selected={selectedIndex === 'users'}
              onClick={() => {
                dispatch({ type: 'users' });
              }}
            >
              <ListItemIcon>
                <PeopleIcon className={classes.dashboardIcon} />
              </ListItemIcon>
              <ListItemText primary={t('Users')} />
            </ListItem>
          </div>
        </LightTooltip>
        <LightTooltip title={open ? '' : t('Settings')} placement="right">
          <div onClick={toggleDrawer ? toggleDrawer(false) : null}>
            <ListItem
              button
              selected={selectedIndex === 'settings'}
              onClick={() => dispatch({ type: 'settings' })}
            >
              <ListItemIcon>
                <SettingsIcon className={classes.dashboardIcon} />
              </ListItemIcon>
              <ListItemText primary={t('Settings')} />
            </ListItem>
          </div>
        </LightTooltip>
      </List>
    </div>
  );
};

export default MainList;
