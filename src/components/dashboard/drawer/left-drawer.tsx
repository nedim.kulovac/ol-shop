import React from 'react';
import clsx from 'clsx';
import Drawer from '@material-ui/core/Drawer';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import Avatar from '@material-ui/core/Avatar';
import { makeStyles } from '@material-ui/core/styles';

import MainList from './main-list';
import StyledBadge from './badge';

const drawerWidth = 260;

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    flex: 1,
    overflowX: 'hidden',
  },
  toolbar: {
    paddingRight: 24, // keep right padding when drawer closed
  },
  companyNameContainer: {
    height: '100%',
    display: 'flex',
    alignItems: 'center',
  },
  companyName: {
    ...theme.typography.button,
    padding: theme.spacing(1),
  },
  toolbarIcon: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: '0 8px',
    ...theme.mixins.toolbar,
  },
  drawerPaper: {
    backgroundColor: '#1b2430',
    color: '#FFF',
    position: 'relative',
    whiteSpace: 'nowrap',
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  dashboardIcon: {
    color: '#FFF',
  },
  chavronIcon: {
    color: '#FFF',
  },
  drawerPaperClose: {
    overflowX: 'hidden',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    width: theme.spacing(7),
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing(9),
    },
  },
  user: {
    marginTop: 'auto',
    backgroundColor: '#121820',
    height: 60,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  avatar: {
    width: 75,
    display: 'flex',
    justifyContent: 'center',
  },
  userContainer: {
    flex: 1,
    display: 'flex',
  },
  userTextContainer: {
    display: 'flex',
    flexDirection: 'column',
  },
}));

interface LeftDrawerProps {
  open: boolean;
  toggleOpen: () => void;
}

const LeftDrawer = ({ open, toggleOpen }: LeftDrawerProps) => {
  const classes = useStyles();
  return (
    <Drawer
      variant="permanent"
      classes={{
        paper: clsx(classes.drawerPaper, !open && classes.drawerPaperClose),
      }}
      open={open}
    >
      <div className={classes.root}>
        <div className={classes.toolbarIcon}>
          <div className={classes.companyNameContainer}>
            <div className={classes.companyName}>{'ELEKTROPRIZMA'}</div>
          </div>
          <IconButton onClick={toggleOpen}>
            <ChevronLeftIcon className={classes.chavronIcon} />
          </IconButton>
        </div>
        <Divider />
        <MainList open={open} />
        <Divider />
        <div className={classes.user}>
          <div className={classes.avatar}>
            <StyledBadge
              overlap="circle"
              anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'right',
              }}
              variant="dot"
            >
              <Avatar alt="Administrator" src="/static/images/avatar/1.jpg" />
            </StyledBadge>
          </div>
          {open && (
            <div className={classes.userContainer}>
              <div className={classes.userTextContainer}>
                <Typography variant="subtitle2">Kulovac Nedim</Typography>
                <Typography variant="caption">Administrator</Typography>
              </div>
            </div>
          )}
        </div>
      </div>
    </Drawer>
  );
};

export default LeftDrawer;
