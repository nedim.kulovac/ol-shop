import React from 'react';
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
import MainList from './main-list';

export default function SwipeableTemporaryDrawer({ toggleDrawer, state }: any) {
  return (
    <div>
      <SwipeableDrawer
        disableDiscovery
        disableSwipeToOpen
        anchor={'left'}
        open={state}
        onClose={toggleDrawer(false)}
        onOpen={toggleDrawer(true)}
      >
        <MainList toggleDrawer={toggleDrawer} />
      </SwipeableDrawer>
    </div>
  );
}
