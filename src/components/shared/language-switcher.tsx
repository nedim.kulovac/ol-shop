import { MenuItem, TextField } from '@material-ui/core';
import { i18n } from 'i18n';
import { useContext } from 'react';
import { I18nContext } from 'next-i18next';

const languages2 = [
  {
    value: 'en',
    label: 'en',
  },
  {
    value: 'bs',
    label: 'bs',
  },
];

const LanguageSwitcher = () => {
  const {
    i18n: { language },
  } = useContext(I18nContext);

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    i18n.changeLanguage(event.target.value);
  };

  return (
    <TextField
      id="standard-select-currency"
      select
      // label="Language"
      value={language}
      onChange={handleChange}
      // helperText="Please select your language"
    >
      {languages2.map((option) => (
        <MenuItem key={option.value} value={option.value}>
          {option.label}
        </MenuItem>
      ))}
    </TextField>
  );
};

export default LanguageSwitcher;
