import { useRouter } from 'next/router';
import { useEffect, useState, useContext, useRef } from 'react';
import { useMutation, useApolloClient } from '@apollo/client';
import { getErrorMessage } from '../../../lib/form';
import { AppContext } from 'src/contexts/AppContext';
import { Field, Form, Formik } from 'formik';
import { object, string } from 'yup';
import { ChangePasswordPayload } from 'src/interfaces';
import ReCAPTCHA from 'react-google-recaptcha';
import Link from 'next/link';
import { ChangePasswordMutation } from '../../gql-helpers/mutations';
import { useTranslation } from 'react-i18next';

import {
  Avatar,
  Button,
  CssBaseline,
  Grid,
  IconButton,
  makeStyles,
  TextField,
  Typography,
  Container,
} from '@material-ui/core';
import Alert from '@material-ui/lab/Alert';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import InputAdornment from '@material-ui/core/InputAdornment';
import MailOutlineIcon from '@material-ui/icons/MailOutline';
import VisibilityOutlinedIcon from '@material-ui/icons/VisibilityOutlined';
import VisibilityOffOutlinedIcon from '@material-ui/icons/VisibilityOffOutlined';

const useStyles = makeStyles((theme) => ({
  container: {
    width: '100%',
  },
  paper: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    width: '100%',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '80%', // Fix IE 11 issue.Favatar
    minWidth: 250,
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  errMsg: {
    width: '80%',
    minWidth: 250,
    marginTop: '10px',
    marginBottom: '15px',
  },
}));

const ChangePassword = () => {
  const classes = useStyles();
  const reRef = useRef<ReCAPTCHA>();
  const router = useRouter();
  const client = useApolloClient();
  const [passwordVisible, setPasswordVisible] = useState(false);

  const { state } = useContext(AppContext);
  const { auth } = state;

  const [changePassword] = useMutation(ChangePasswordMutation);
  const [errorMsg, setErrorMsg] = useState(null);
  const forgotPasswordMode = router.asPath.endsWith('tempPassReq');
  const [flag, setFlag] = useState('');
  const { t } = useTranslation('auth');

  useEffect(() => {
    if (!state.auth?.authenticatedUser && !forgotPasswordMode) {
      router.push('/auth/[form]', '/auth/sign-out');
    } else if (!state.auth?.authenticatedUser && forgotPasswordMode) {
      setFlag('forgotPassword');
    } else if (state.auth?.authenticatedUser && !forgotPasswordMode) {
      setFlag('changePassword');
    }
  }, []);

  async function handleSubmit({
    email,
    password,
    newPassword,
  }: ChangePasswordPayload) {
    setErrorMsg(null);
    try {
      await client.clearStore();
      const { data } = await changePassword({
        variables: {
          email,
          password,
          newPassword,
          flag,
        },
      });
      if (data.changePassword) {
        router.push('/auth/[form]', '/auth/sign-out?password-changed-end');
      }
    } catch (error) {
      setErrorMsg(t(getErrorMessage(error)));
    }
  }

  const initialValues: ChangePasswordPayload = {
    email: forgotPasswordMode ? '' : state.auth?.authenticatedUser.email,
    password: '',
    confirmPassword: '',
    newPassword: '',
  };

  return (
    <div className={classes.container}>
      <Container component="main" maxWidth="md">
        <CssBaseline />
        <div className={classes.paper}>
          <Avatar className={classes.avatar}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography
            component="h1"
            variant="h5"
            gutterBottom
            style={{ paddingBottom: 20 }}
          >
            {t('Change password')}
          </Typography>
          {errorMsg && (
            <Alert
              variant="filled"
              severity="error"
              className={classes.errMsg}
              onClose={() => {
                setErrorMsg(null);
              }}
            >
              {errorMsg}
            </Alert>
          )}
          {forgotPasswordMode && (
            <Typography variant="body1">
              {t('Please enter new password in order to use application')}
            </Typography>
          )}
          <Formik
            initialValues={initialValues}
            onSubmit={(values) => handleSubmit(values)}
            validationSchema={object({
              email: string().required().email(),
              password: string().required().min(8),
              newPassword: string().required().min(8),
            })}
          >
            {({ errors, touched, values }) => (
              <Form className={classes.form}>
                {forgotPasswordMode && (
                  <Field
                    as={TextField}
                    name="email"
                    type="email"
                    margin="normal"
                    required
                    fullWidth
                    id="email"
                    label={t('Email Address')}
                    autoComplete="email"
                    error={!!touched.email && !!errors.email}
                    helperText={
                      touched.email && errors.email ? t(errors.email) : null
                    }
                    InputProps={{
                      endAdornment: (
                        <InputAdornment position="end">
                          <MailOutlineIcon />
                        </InputAdornment>
                      ),
                    }}
                  />
                )}
                <Field
                  as={TextField}
                  name="password"
                  type={passwordVisible ? 'text' : 'password'}
                  margin="normal"
                  required
                  fullWidth
                  id="password"
                  label={
                    !forgotPasswordMode
                      ? t('Old Password')
                      : t('Password you got in email')
                  }
                  autoComplete="password"
                  error={!!touched.password && !!errors.password}
                  helperText={
                    touched.password && errors.password
                      ? t(errors.password)
                      : null
                  }
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">
                        <IconButton
                          aria-label="toggle password visibility"
                          onClick={() =>
                            setPasswordVisible((isVisible) => !isVisible)
                          }
                          edge="end"
                        >
                          {passwordVisible ? (
                            <VisibilityOutlinedIcon />
                          ) : (
                            <VisibilityOffOutlinedIcon />
                          )}
                        </IconButton>
                      </InputAdornment>
                    ),
                  }}
                />
                <Field
                  as={TextField}
                  name="newPassword"
                  type={passwordVisible ? 'text' : 'password'}
                  margin="normal"
                  required
                  fullWidth
                  id="newPassword"
                  label={t('New Password')}
                  autoComplete="password"
                  error={!!touched.newPassword && !!errors.newPassword}
                  helperText={
                    touched.newPassword && errors.newPassword
                      ? t(errors.newPassword)
                      : null
                  }
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">
                        <IconButton
                          aria-label="toggle password visibility"
                          onClick={() =>
                            setPasswordVisible((isVisible) => !isVisible)
                          }
                          edge="end"
                        >
                          {passwordVisible ? (
                            <VisibilityOutlinedIcon />
                          ) : (
                            <VisibilityOffOutlinedIcon />
                          )}
                        </IconButton>
                      </InputAdornment>
                    ),
                  }}
                />
                <Field
                  as={TextField}
                  name="confirmPassword"
                  type={passwordVisible ? 'text' : 'password'}
                  margin="normal"
                  required
                  fullWidth
                  id="confirmPassword"
                  label={t('Confirm Password')}
                  autoComplete="password"
                  error={values.newPassword !== values.confirmPassword}
                  helperText={
                    values.newPassword !== values.confirmPassword
                      ? t('Does not match entered password')
                      : null
                  }
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">
                        <IconButton
                          aria-label="toggle password visibility"
                          onClick={() =>
                            setPasswordVisible((isVisible) => !isVisible)
                          }
                          edge="end"
                        >
                          {passwordVisible ? (
                            <VisibilityOutlinedIcon />
                          ) : (
                            <VisibilityOffOutlinedIcon />
                          )}
                        </IconButton>
                      </InputAdornment>
                    ),
                  }}
                />
                <ReCAPTCHA
                  sitekey={process.env.NEXT_PUBLIC_RECAPTCHA_SITE_KEY}
                  size="invisible"
                  ref={reRef}
                />
                <Button
                  type="submit"
                  fullWidth
                  variant="contained"
                  color="primary"
                  className={classes.submit}
                  disabled={
                    (Object.keys(errors).length !== 0 &&
                      errors.constructor === Object) ||
                    (Object.keys(touched).length === 0 &&
                      touched.constructor === Object) ||
                    values.newPassword !== values.confirmPassword
                  }
                >
                  {t('Reset password')}
                </Button>
                <Grid container>
                  <Grid item xs>
                    <Link href="/auth/[form]" as="/auth/forgot-password">
                      <a>{t('Forgot password')}</a>
                    </Link>
                  </Grid>
                  <Grid item>
                    <Link href="/auth/[form]" as={`/auth/sign-in`}>
                      <a>{t('You already have an account? Sign In')}</a>
                    </Link>
                  </Grid>
                </Grid>
              </Form>
            )}
          </Formik>
        </div>
      </Container>
    </div>
  );
};

export default ChangePassword;
