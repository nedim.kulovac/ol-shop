import { useState, useContext, useRef, useEffect } from 'react';
import { useRouter } from 'next/router';
// import Link from 'next/link';
import { useLazyQuery } from '@apollo/client';
import { useMutation, useApolloClient } from '@apollo/client';
import { getErrorMessage } from '../../../lib/form';
import { AppContext } from '../../contexts/AppContext';
import { Types } from '../../contexts/reducers';
import { CurrentUser } from '../../gql-helpers/queries';
import { SigninMutation } from '../../gql-helpers/mutations';
import { Link } from 'i18n';
import { I18nContext } from 'next-i18next';
import { useTranslation } from 'react-i18next';

import ReCAPTCHA from 'react-google-recaptcha';
import { Form, Formik, Field } from 'formik';
import { object, string } from 'yup';
import { SigninPayload } from 'src/interfaces';

import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Alert from '@material-ui/lab/Alert';
import InputAdornment from '@material-ui/core/InputAdornment';
import MailOutlineIcon from '@material-ui/icons/MailOutline';
import VisibilityOutlinedIcon from '@material-ui/icons/VisibilityOutlined';
import VisibilityOffOutlinedIcon from '@material-ui/icons/VisibilityOffOutlined';
import { CircularProgress, IconButton } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  container: {
    width: '100%',
  },
  paper: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    width: '100%',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '80%', // Fix IE 11 issue.Favatar
    minWidth: 250,
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  errMsg: {
    width: '80%',
    minWidth: 250,
    marginTop: '10px',
    marginBottom: '15px',
  },
}));

const Signin = () => {
  const classes = useStyles();
  const router = useRouter();
  const reRef = useRef<ReCAPTCHA>();
  const client = useApolloClient();
  const [loading, setLoading] = useState(true);
  const [getData, { data }] = useLazyQuery(CurrentUser);
  const [signIn] = useMutation(SigninMutation);
  const [errorMsg, setErrorMsg] = useState(null);
  const [succMsg, setSuccMsg] = useState(null);
  const [passwordVisible, setPasswordVisible] = useState(false);
  const { state, dispatch } = useContext(AppContext);
  const {
    i18n: { language },
  } = useContext(I18nContext);
  const { t } = useTranslation('auth');

  useEffect(() => {
    getData();
    if (data?.currentUser) {
      dispatch({
        type: Types.SetAuthenticatedUser,
        payload: data.currentUser,
      });
      router.push('/dashboard');
    } else setLoading(false);
  }, []);

  useEffect(() => {
    if (router.asPath.endsWith('password-changed'))
      setSuccMsg(t('password-changed'));
    if (router.asPath.endsWith('password-changed-end'))
      setSuccMsg(t('password-changed-end'));
    else if (router.asPath.endsWith('signup-success'))
      setSuccMsg(t('signup-success'));
    else if (router.asPath.endsWith('account-verified'))
      setSuccMsg(t('account-verified'));
  }, [language]);

  async function handleSubmit({
    email,
    password,
    rcToken,
  }: SigninPayload): Promise<void> {
    try {
      setSuccMsg(null);
      setErrorMsg(null);
      rcToken = await reRef.current.executeAsync();
      reRef.current.reset();
      const { data } = await signIn({
        variables: {
          email,
          password,
          rcToken,
        },
      });
      if (data.signin.tempPassReq) {
        await router.push('/auth/[form]', '/auth/change-password?tempPassReq');
      } else if (data.signin._id) {
        dispatch({
          type: Types.SetAuthenticatedUser,
          payload: data.signin,
        });
        await router.push('/dashboard');
      }
      client.resetStore();
    } catch (error) {
      setErrorMsg(t(getErrorMessage(error)));
    }
  }

  const initialValues: SigninPayload = {
    email: '',
    password: '',
    rcToken: '',
  };

  if (loading) return <CircularProgress />;

  return (
    <div className={classes.container}>
      <Container component="main" maxWidth="md">
        <CssBaseline />
        <div className={classes.paper}>
          <Avatar className={classes.avatar}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            {t('Sign In')}
          </Typography>
          {errorMsg && (
            <Alert
              variant="filled"
              severity="error"
              className={classes.errMsg}
              onClose={() => {
                setErrorMsg(null);
              }}
            >
              {errorMsg}
            </Alert>
          )}
          {succMsg && (
            <Alert
              variant="filled"
              severity="success"
              className={classes.errMsg}
              onClose={() => {
                setSuccMsg(null);
              }}
            >
              {succMsg}
            </Alert>
          )}
          <Formik
            initialValues={initialValues}
            onSubmit={(values) => handleSubmit(values)}
            validationSchema={object({
              email: string().required().email(),
              password: string().required().min(8),
            })}
          >
            {({ errors, touched }) => (
              <Form className={classes.form}>
                <Field
                  as={TextField}
                  name="email"
                  type="email"
                  margin="normal"
                  required
                  fullWidth
                  id="email"
                  label={t('Email Address')}
                  autoComplete="email"
                  error={!!touched.email && !!errors.email}
                  helperText={
                    touched.email && errors.email ? t(errors.email) : null
                  }
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">
                        <MailOutlineIcon />
                      </InputAdornment>
                    ),
                  }}
                />
                <Field
                  as={TextField}
                  margin="normal"
                  required
                  fullWidth
                  name="password"
                  label={t('Password')}
                  type={passwordVisible ? 'text' : 'password'}
                  id="password"
                  autoComplete="current-password"
                  error={!!touched.password && !!errors.password}
                  helperText={
                    touched.password && errors.password
                      ? t(errors.password)
                      : null
                  }
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">
                        <IconButton
                          aria-label="toggle password visibility"
                          onClick={() =>
                            setPasswordVisible((isVisible) => !isVisible)
                          }
                          edge="end"
                        >
                          {passwordVisible ? (
                            <VisibilityOutlinedIcon />
                          ) : (
                            <VisibilityOffOutlinedIcon />
                          )}
                        </IconButton>
                      </InputAdornment>
                    ),
                  }}
                />
                <ReCAPTCHA
                  sitekey={process.env.NEXT_PUBLIC_RECAPTCHA_SITE_KEY}
                  size="invisible"
                  ref={reRef}
                />
                <Button
                  type="submit"
                  fullWidth
                  variant="contained"
                  color="primary"
                  className={classes.submit}
                  disabled={
                    (Object.keys(errors).length !== 0 &&
                      errors.constructor === Object) ||
                    (Object.keys(touched).length === 0 &&
                      touched.constructor === Object)
                  }
                >
                  {t('Sign In')}
                </Button>
                <Grid container>
                  <Grid item xs>
                    <Link href="/auth/[form]" as="/auth/forgot-password">
                      <a>{t('Forgot password')}</a>
                    </Link>
                  </Grid>
                  <Grid item>
                    <Link href="/auth/[form]" as="/auth/sign-up">
                      <a>{t("Don't have an account? Sign Up")}</a>
                    </Link>
                  </Grid>
                </Grid>
              </Form>
            )}
          </Formik>
        </div>
      </Container>
    </div>
  );
};

export default Signin;
