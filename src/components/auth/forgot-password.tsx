import { useContext, useEffect, useRef } from 'react';
import { useMutation, useApolloClient, useLazyQuery } from '@apollo/client';
import { getErrorMessage } from '../../../lib/form';
import { useState } from 'react';
import Link from 'next/link';
import ReCAPTCHA from 'react-google-recaptcha';
import { Form, Formik, Field } from 'formik';
import { object, string } from 'yup';
import { ResetPasswordPayload } from 'src/interfaces';
import { useRouter } from 'next/router';
import { CurrentUser } from '../../gql-helpers/queries';
import { ResetPasswordMutation } from '../../gql-helpers/mutations';
import { useTranslation } from 'react-i18next';
import { I18nContext } from 'next-i18next';

import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import HelpOutlineOutlinedIcon from '@material-ui/icons/HelpOutlineOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Alert from '@material-ui/lab/Alert';
import InputAdornment from '@material-ui/core/InputAdornment';
import MailOutlineIcon from '@material-ui/icons/MailOutline';
import { Types } from 'src/contexts/reducers';
import { AppContext } from 'src/contexts/AppContext';
import { CircularProgress } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  container: {
    width: '100%',
  },
  paper: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    width: '100%',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '80%', // Fix IE 11 issue.Favatar
    minWidth: 250,
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  errMsg: {
    width: '80%',
    minWidth: 250,
    marginTop: '10px',
    marginBottom: '15px',
  },
}));

const ForgotPassword = () => {
  const classes = useStyles();
  const reRef = useRef<ReCAPTCHA>();
  const client = useApolloClient();
  const [resetPassword] = useMutation(ResetPasswordMutation);
  const [errorMsg, setErrorMsg] = useState();
  const router = useRouter();
  const [loading, setLoading] = useState(true);
  const [getData, { data }] = useLazyQuery(CurrentUser);
  const { state, dispatch } = useContext(AppContext);
  const { t } = useTranslation('auth');
  const {
    i18n: { language },
  } = useContext(I18nContext);

  useEffect(() => {
    getData();
    if (data?.currentUser) {
      dispatch({
        type: Types.SetAuthenticatedUser,
        payload: data.currentUser,
      });
      router.push('/dashboard');
    } else setLoading(false);
  }, [data]);

  async function handleSubmit({
    email,
    rcToken,
    language,
  }: ResetPasswordPayload) {
    try {
      client.clearStore();
      setErrorMsg(null);
      rcToken = await reRef.current.executeAsync();
      reRef.current.reset();
      const { data } = await resetPassword({
        variables: {
          email,
          rcToken,
          language,
        },
      });
      if (data.resetPassword) {
        await router.push('/auth/[form]', '/auth/sign-in?password-changed');
      }
    } catch (error) {
      setErrorMsg(t(getErrorMessage(error)));
    }
  }

  const initialValues: ResetPasswordPayload = {
    email: ' ',
    rcToken: '',
    language,
  };

  if (loading) return <CircularProgress />;

  return (
    <div className={classes.container}>
      <Container component="main" maxWidth="md">
        <CssBaseline />
        <div className={classes.paper}>
          <Avatar className={classes.avatar}>
            <HelpOutlineOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            {t('Forgot password')}?
          </Typography>
          {errorMsg && (
            <Alert
              variant="filled"
              severity="error"
              className={classes.errMsg}
              onClose={() => {
                setErrorMsg(null);
              }}
            >
              {errorMsg}
            </Alert>
          )}
          <Formik
            initialValues={initialValues}
            onSubmit={(values) => handleSubmit(values)}
            validationSchema={object({
              email: string().required().email(),
            })}
          >
            {({ errors, touched, setTouched }) => (
              useEffect(() => {
                setTouched({ email: false }, true);
              }, [initialValues]),
              (
                <Form className={classes.form}>
                  <Field
                    as={TextField}
                    name="email"
                    type="email"
                    margin="normal"
                    required
                    fullWidth
                    id="email"
                    label={t('Email Address')}
                    autoComplete="email"
                    error={!!touched.email && !!errors.email}
                    helperText={
                      touched.email && errors.email ? t(errors.email) : null
                    }
                    InputProps={{
                      endAdornment: (
                        <InputAdornment position="end">
                          <MailOutlineIcon />
                        </InputAdornment>
                      ),
                    }}
                  />
                  <ReCAPTCHA
                    sitekey={process.env.NEXT_PUBLIC_RECAPTCHA_SITE_KEY}
                    size="invisible"
                    ref={reRef}
                  />
                  <Button
                    type="submit"
                    fullWidth
                    variant="contained"
                    color="primary"
                    className={classes.submit}
                    disabled={
                      (Object.keys(errors).length !== 0 &&
                        errors.constructor === Object) ||
                      (Object.keys(touched).length === 0 &&
                        touched.constructor === Object)
                    }
                  >
                    {t('Reset password')}
                  </Button>
                  <Grid
                    container
                    alignItems="flex-start"
                    justify="flex-end"
                    direction="row"
                  >
                    <Link href="/auth/[form]" as={`/auth/sign-in`}>
                      <a>{t('You already have an account? Sign In')}</a>
                    </Link>
                  </Grid>
                </Form>
              )
            )}
          </Formik>
        </div>
      </Container>
    </div>
  );
};

export default ForgotPassword;
