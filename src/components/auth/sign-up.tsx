import React, { useContext, useEffect } from 'react';
import { useState, useRef } from 'react';
import { useRouter } from 'next/router';
import Link from 'next/link';
import { useApolloClient, useLazyQuery, useMutation } from '@apollo/client';
import { getErrorMessage } from '../../../lib/form';
import ReCAPTCHA from 'react-google-recaptcha';
import { Form, Formik, Field } from 'formik';
import { object, string } from 'yup';
import { SignupPayload } from '../../interfaces';
import { CurrentUser } from '../../gql-helpers/queries';
import { SignupMutation } from '../../gql-helpers/mutations';
import { useTranslation } from 'react-i18next';
import { I18nContext } from 'next-i18next';

import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Alert from '@material-ui/lab/Alert';
import InputAdornment from '@material-ui/core/InputAdornment';
import IconButton from '@material-ui/core/IconButton';
import MailOutlineIcon from '@material-ui/icons/MailOutline';
import VisibilityOutlinedIcon from '@material-ui/icons/VisibilityOutlined';
import VisibilityOffOutlinedIcon from '@material-ui/icons/VisibilityOffOutlined';
import CheckOutlinedIcon from '@material-ui/icons/CheckOutlined';
import ClearOutlinedIcon from '@material-ui/icons/ClearOutlined';
import PersonOutlineOutlinedIcon from '@material-ui/icons/PersonOutlineOutlined';
import { Types } from 'src/contexts/reducers';
import { AppContext } from 'src/contexts/AppContext';
import { CircularProgress } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  container: {
    width: '100%',
  },
  paper: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    width: '100%',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '80%', // Fix IE 11 issue.Favatar
    minWidth: 250,
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  errMsg: {
    width: '80%',
    minWidth: 250,
    marginTop: '10px',
    marginBottom: '25px',
  },
}));

const Signup = () => {
  const classes = useStyles();
  const [signup] = useMutation(SignupMutation);
  const [errorMsg, setErrorMsg] = useState();
  const router = useRouter();
  const reRef = useRef<ReCAPTCHA>();
  const [passwordVisible, setPasswordVisible] = useState(false);
  const client = useApolloClient();
  const { state, dispatch } = useContext(AppContext);
  const [loading, setLoading] = useState(true);
  const [getData, { data }] = useLazyQuery(CurrentUser);
  const { t } = useTranslation('auth');
  const {
    i18n: { language },
  } = useContext(I18nContext);

  useEffect(() => {
    getData();
    if (data?.currentUser) {
      dispatch({
        type: Types.SetAuthenticatedUser,
        payload: data.currentUser,
      });
      router.push('/dashboard');
    } else setLoading(false);
  }, []);

  async function handleSubmit({
    firstName,
    lastName,
    email,
    password,
    rcToken,
    language,
  }: SignupPayload) {
    try {
      client.clearStore();
      setErrorMsg(null);
      rcToken = await reRef.current.executeAsync();
      reRef.current.reset();
      const res = await signup({
        variables: {
          firstName,
          lastName,
          email,
          password,
          rcToken,
          language,
        },
      });
      if (res.data.signup._id)
        router.push('/auth/[form]', '/auth/sign-in?signup-success');
    } catch (error) {
      setErrorMsg(t(getErrorMessage(error)));
    }
  }

  const initialValues: SignupPayload = {
    firstName: '',
    lastName: '',
    email: '',
    password: '',
    confirmPassword: '',
    rcToken: '',
    language,
  };

  if (loading) return <CircularProgress />;

  return (
    <div className={classes.container}>
      <Container component="main" maxWidth="md">
        <CssBaseline />
        <div className={classes.paper}>
          <Avatar className={classes.avatar}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5" gutterBottom>
            {t('Sign Up')}
          </Typography>
          {errorMsg && (
            <Alert
              variant="filled"
              severity="error"
              className={classes.errMsg}
              onClose={() => {
                setErrorMsg(null);
              }}
            >
              {errorMsg}
            </Alert>
          )}
          <Formik
            initialValues={initialValues}
            onSubmit={(values) => handleSubmit(values)}
            validationSchema={object({
              firstName: string().trim().required('First name is required'),
              lastName: string().trim().required('Last name is required'),
              email: string().required().email(),
              password: string().required().min(8),
            })}
          >
            {({ errors, touched, values }) => (
              <Form className={classes.form}>
                <Grid container spacing={2}>
                  <Grid item xs={12} md={6}>
                    <Field
                      as={TextField}
                      autoComplete="fname"
                      name="firstName"
                      required
                      fullWidth
                      id="firstName"
                      label={t('First Name')}
                      error={!!touched.firstName && !!errors.firstName}
                      helperText={
                        touched.firstName && errors.firstName
                          ? t(errors.firstName)
                          : null
                      }
                      InputProps={{
                        endAdornment: (
                          <InputAdornment position="end">
                            <PersonOutlineOutlinedIcon />
                          </InputAdornment>
                        ),
                      }}
                    />
                  </Grid>
                  <Grid item xs={12} md={6}>
                    <Field
                      as={TextField}
                      required
                      fullWidth
                      id="lastName"
                      label={t('Last Name')}
                      name="lastName"
                      autoComplete="lname"
                      error={!!touched.lastName && !!errors.lastName}
                      helperText={
                        touched.lastName && errors.lastName
                          ? t(errors.lastName)
                          : null
                      }
                      InputProps={{
                        endAdornment: (
                          <InputAdornment position="end">
                            <PersonOutlineOutlinedIcon />
                          </InputAdornment>
                        ),
                      }}
                    />
                  </Grid>
                </Grid>
                <Field
                  as={TextField}
                  name="email"
                  type="email"
                  margin="normal"
                  required
                  fullWidth
                  id="email"
                  label={t('Email Address')}
                  autoComplete="email"
                  error={!!touched.email && !!errors.email}
                  helperText={
                    touched.email && errors.email ? t(errors.email) : null
                  }
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">
                        <MailOutlineIcon />
                      </InputAdornment>
                    ),
                  }}
                />
                <Field
                  as={TextField}
                  margin="normal"
                  required
                  fullWidth
                  name="password"
                  label={t('Password')}
                  type={passwordVisible ? 'text' : 'password'}
                  id="password"
                  autoComplete="password"
                  error={!!touched.password && !!errors.password}
                  helperText={
                    touched.password && errors.password
                      ? t(errors.password)
                      : null
                  }
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">
                        <IconButton
                          aria-label="toggle password visibility"
                          onClick={() =>
                            setPasswordVisible((isVisible) => !isVisible)
                          }
                          edge="end"
                        >
                          {passwordVisible ? (
                            <VisibilityOutlinedIcon />
                          ) : (
                            <VisibilityOffOutlinedIcon />
                          )}
                        </IconButton>
                      </InputAdornment>
                    ),
                  }}
                />
                <Field
                  as={TextField}
                  margin="normal"
                  required
                  fullWidth
                  name="confirmPassword"
                  label={t('Confirm Password')}
                  type={passwordVisible ? 'text' : 'password'}
                  id="confirmPassword"
                  autoComplete="password"
                  error={values.password !== values.confirmPassword}
                  helperText={
                    values.password !== values.confirmPassword
                      ? t('Does not match entered password')
                      : null
                  }
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">
                        {values.password === values.confirmPassword ? (
                          <CheckOutlinedIcon />
                        ) : (
                          <ClearOutlinedIcon />
                        )}
                      </InputAdornment>
                    ),
                  }}
                />
                <ReCAPTCHA
                  sitekey={process.env.NEXT_PUBLIC_RECAPTCHA_SITE_KEY}
                  size="invisible"
                  ref={reRef}
                />
                <Button
                  type="submit"
                  fullWidth
                  variant="contained"
                  color="primary"
                  className={classes.submit}
                  disabled={
                    (Object.keys(errors).length !== 0 &&
                      errors.constructor === Object) ||
                    (Object.keys(touched).length === 0 &&
                      touched.constructor === Object) ||
                    values.password !== values.confirmPassword
                  }
                >
                  {t('Sign Up')}
                </Button>
                <Grid
                  container
                  alignItems="flex-start"
                  justify="flex-end"
                  direction="row"
                >
                  <Link href="/auth/[form]" as={`/auth/sign-in`}>
                    <a>{t('You already have an account? Sign In')}</a>
                  </Link>
                </Grid>
              </Form>
            )}
          </Formik>
        </div>
      </Container>
    </div>
  );
};

export default Signup;
