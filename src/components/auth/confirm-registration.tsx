import { useMutation } from '@apollo/client';
import { Button, CircularProgress, Grid, Typography } from '@material-ui/core';
import Alert from '@material-ui/lab/Alert';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';
import { getErrorMessage } from '../../../lib/form';
import { ConfirmUserRegistration } from '../../gql-helpers/mutations';
import { useTranslation } from 'react-i18next';

const ConfirmRegistration = ({ email, randString }) => {
  const [loading, setLoading] = useState(true);
  const router = useRouter();
  const [confirmationMutation] = useMutation(ConfirmUserRegistration);
  const [errorMsg, setErrorMsg] = useState(null);
  const { t } = useTranslation('auth');

  useEffect(() => {
    const call = async () => {
      try {
        const { data } = await confirmationMutation({
          variables: {
            email,
            randString,
          },
        });
        if (data.confirmUserRegistration)
          await router.push('/auth/[form]', '/auth/sign-in?account-verified');
      } catch (error) {
        setErrorMsg(t(getErrorMessage(error)));
        setLoading(false);
      }
    };
    call();
  }, []);

  return (
    <div style={{ width: '80%' }}>
      <Typography
        component="h1"
        variant="h5"
        gutterBottom
        style={{ paddingBottom: 20 }}
      >
        {t('Email verification')}
      </Typography>
      {loading && <CircularProgress /> && (
        <Typography component="h1" variant="h5" gutterBottom>
          {t('Waiting confirmation')}
        </Typography>
      )}
      {errorMsg && (
        <div>
          <Alert
            variant="filled"
            severity="error"
            onClose={() => {
              setErrorMsg(null);
            }}
          >
            {errorMsg}
          </Alert>
          <Grid container spacing={2}>
            <Grid item sm={12} md={6}>
              <Button
                variant="contained"
                color="primary"
                fullWidth
                style={{ marginTop: 25 }}
                onClick={() => {
                  router.push('/auth/[form]', `/auth/sign-in`);
                }}
              >
                <a>{t('Sign In')}</a>
              </Button>
            </Grid>
            <Grid item sm={12} md={6}>
              <Button
                variant="contained"
                color="primary"
                fullWidth
                style={{ marginTop: 25 }}
                onClick={() => {
                  router.push('/auth/[form]', `/auth/sign-up`);
                }}
              >
                <a>{t('Sign Up')}</a>
              </Button>
            </Grid>
          </Grid>
        </div>
      )}
    </div>
  );
};

export default ConfirmRegistration;
