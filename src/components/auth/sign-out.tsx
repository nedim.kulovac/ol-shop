import { useEffect, useContext } from 'react';
import { useRouter } from 'next/router';
import { useMutation, useApolloClient } from '@apollo/client';
import { AppContext } from 'src/contexts/AppContext';
import { Types } from 'src/contexts/reducers';
import { CircularProgress } from '@material-ui/core';
import { SigoutMutation } from '../../gql-helpers/mutations';

const Signout = () => {
  const { dispatch } = useContext(AppContext);
  const client = useApolloClient();
  const router = useRouter();
  const [signOut] = useMutation(SigoutMutation);

  useEffect(() => {
    signOut().then(() => {
      client.resetStore().then(() => {
        dispatch({ type: Types.SetAuthenticatedUser, payload: null });
        if (router.asPath.endsWith('password-changed-end'))
          router.push('/auth/[form]', '/auth/sign-in?password-changed-end');
        else router.push('/auth/[form]', '/auth/sign-in');
      });
    });
  }, [signOut, router, client]);

  return <CircularProgress />;
};

export default Signout;
