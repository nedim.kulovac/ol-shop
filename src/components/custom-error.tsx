import { Button, Grid } from '@material-ui/core';
import { useRouter } from 'next/router';
import { useTranslation } from 'react-i18next';

export default function CustomError() {
  const router = useRouter();
  const { t } = useTranslation('auth');
  return (
    <Grid container>
      <Grid item xs={12}>
        <h1
          style={{
            fontSize: 100,
            color: 'GrayText',
            marginBottom: 0,
            textAlign: 'center',
          }}
        >
          500
        </h1>
      </Grid>
      <Grid item xs={12}>
        <h1
          style={{
            color: 'GrayText',
            textAlign: 'center',
          }}
        >
          Ooops!!!
        </h1>
      </Grid>
      <Grid item xs={12}>
        <h2
          style={{
            color: 'GrayText',
            textTransform: 'uppercase',
            marginBottom: 50,
            textAlign: 'center',
          }}
        >
          {t('Something went wrong')}
        </h2>
      </Grid>
      <Grid item xs={12} style={{ display: 'flex', justifyContent: 'center' }}>
        <div
          style={{
            width: '60%',
            minWidth: 200,
            marginTop: 20,
            marginBottom: 100,
          }}
        >
          <Button
            variant="contained"
            color="primary"
            fullWidth
            onClick={() => {
              router.push('/dashboard');
            }}
          >
            {t('Go home')}
          </Button>
        </div>
      </Grid>
    </Grid>
  );
}
